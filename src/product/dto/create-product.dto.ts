import { IsNotEmpty, MinLength } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @MinLength(3)
  price: string;
}
