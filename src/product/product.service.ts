import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let product: Product[] = [
  { id: 1, name: 'product1', price: 200 },
  { id: 2, name: 'product2', price: 100 },
  { id: 3, name: 'product3', price: 300 },
];
let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...CreateProductDto,
    };
    product.push(newProduct);
    return newProduct;
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return product[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...product[index],
      ...updateProductDto,
    };
    product[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = product.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedUser = product[index];
    product.splice(index, 1);
    return deletedUser;
  }

  reset() {
    product = [
      { id: 1, name: 'product1', price: 200 },
      { id: 2, name: 'product2', price: 100 },
      { id: 3, name: 'product3', price: 300 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
